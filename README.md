# Cudimot-models

Cudimot allows for computationally efficient diffusion model estimation using GPUs. For more details please see https://users.fmrib.ox.ac.uk/~moisesf/cudimot/ or Hernandez-Fernandez M., Reguly I., Jbabdi S, Giles M, Smith S., Sotiropoulos S.N. "Using GPUs to accelerate computational diffusion MRI: From microstructure estimation to tractography and connectomes." NeuroImage 188 (2019): 598-615.

Here we provide edited cudimot files to run the NODDI model (either in vivo or ex vivo with a dot compartment) using global, user specified diffusivities. Both models can be run using the wrapper function run_NODDI.sh using the -m invivo/exvivo option.

Note, if using magnitude data, it is often recommended that you use a Rician noise model which can be specified by the --rician flag. MCMC optimisation, to provide parameter distributions, can be run using --runMCMC.

If using this code please remember to cite Hernandez-Fernandez et al. Neuroimage 2019 as well as the original NODDI paper, Zhang et al. Neuroimage 2012.

For further discussion of axial diffusivity in the NODDI model, including how the choice of axial diffusivity affects the estimated parameter maps, please see Howard et al., Estimating axial diffusivity in the NODDI model. Neuroimage. 2022  doi: 10.1016/j.neuroimage.2022.119535
